#!/bin/bash
dir='./doc'

if [ -d $dir ]; then
    rm -rf $dir/*
else
    mkdir $dir
fi

for file in *.py; do
    pydoc -w ${file%.*}
done

pydoc -w core
pydoc -w core.gui
pydoc -w widgets

for file in core/*.py; do
    file=${file%.*}
    file=${file//\//.}
    pydoc -w $file
done

for file in core/gui/*.py; do
    file=${file%.*}
    file=${file//\//.}
    pydoc -w $file
done

for file in widgets/*.py; do
    file=${file%.*}
    file=${file//\//.}
    pydoc -w $file
done

mv *.html $dir/
